from dataclasses import dataclass

from fastapi import FastAPI

app = FastAPI()


@dataclass
class Item:
    name: str
    price: int


d = [Item("ice", 10)]


@app.get("/")
def root():
    return {"message": "Привет, это корневой эндпоинт!"}


@app.get("/items/{item_id}")
def get_item(item_id: int):
    cur_value = d[item_id]
    return {"item_id": item_id, "Название товара": cur_value.name, "price": cur_value.price}


@app.post("/items")
def create_item(item: dict):
    name = item.get("name")
    price = item.get("price")
    i = Item(name, price)
    d.append(i)

    return {"message": "Item успешно создан", "item_id": len(d), "name": name, "price": price}


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="localhost", port=8000)

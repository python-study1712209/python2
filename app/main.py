from enum import Enum as PyEnum
from typing import Optional

import uvicorn
from fastapi import FastAPI, Request, Depends, status, HTTPException, Form
from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from loguru import logger
from pydantic_sqlalchemy import sqlalchemy_to_pydantic
from sqlalchemy import create_engine, Column, Integer, Boolean, Text, Enum
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session, sessionmaker

Base = declarative_base()


class TodoStatus(str, PyEnum):
    STUDY = 'STUDY'
    PERSONAL = 'PERSONAL'
    PLANS = 'PLANS'


class TodoItem(Base):
    """Todo model
    """
    __tablename__ = 'todos'
    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(Text)
    details = Column(Text, nullable=True)
    completed = Column(Boolean, default=False)
    tag = Column(Enum(TodoStatus))

    def __repr__(self):
        return f'<Todo {self.id}>'


TodoItemData = sqlalchemy_to_pydantic(TodoItem)

# class CreateTodoRequest(BaseModel):
#     data: TodoItemData
#
#     @validator('data')
#     def validate_data(cls, value):
#         if not value.title.strip():
#             raise ValueError('Title should not be empty')
#         return value


# Database for todo
DB_URL = "sqlite:///./db.sqlite"
ENGINE = create_engine(DB_URL, connect_args={"check_same_thread": False})
SESSIONLOCAL = sessionmaker(autocommit=False, autoflush=False, bind=ENGINE)


def init_db():
    """Init database, create all models as tables
    """
    Base.metadata.create_all(bind=ENGINE)


def get_db():
    """Create session/connection for each request
    """
    database = SESSIONLOCAL()
    try:
        yield database
    finally:
        database.close()


# Main of todo app
init_db()

# Create the FastAPI app
app = FastAPI()

# Create the Jinja2 templates
templates = Jinja2Templates(directory="templates")

# Loguru configuration
logger.configure(handlers=[{"sink": "app.log", "level": "DEBUG", "serialize": True}])

app.mount("/static", StaticFiles(directory="static"), name="static")


# Routes
@app.get("/")
async def home(request: Request, database: Session = Depends(get_db), offset: int = 0, limit: int = 10):
    """Main page with todo list"""
    total_todos = database.query(TodoItem).count()
    # offset = (page - 1) * limit
    todos = database.query(TodoItem).order_by(TodoItem.id.desc()).offset(offset).limit(limit).all()
    return templates.TemplateResponse("index.html",
                                      {"request": request, "tags": TodoStatus, "todos": todos,
                                       "limit": limit, "total_todos": total_todos, "offset": offset})


@app.post("/add")
async def todo_add(request: Request, database: Session = Depends(get_db)):
    """Add new todo"""
    todo_data = await request.form()

    title = todo_data.get("title")
    details = todo_data.get("details")
    tag = todo_data.get("tag")

    if len(details) > 500 or len(title) < 3:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Title length exceeds the limit")

    todo = TodoItem(title=title, details=details, completed=False, tag=tag)

    database.add(todo)
    database.commit()

    return RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@app.get("/edit/{todo_id}")
async def todo_edit(request: Request, todo_id: int, database: Session = Depends(get_db)):
    """Edit todo page"""
    todo = database.query(TodoItem).get(todo_id)
    if not todo:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Todo not found")
    return templates.TemplateResponse("edit.html", {"request": request, "todo": todo, "tags": TodoStatus})


@app.post("/edit/{todo_id}")
async def todo_update(request: Request, todo_id: int, title: str = Form(...), details: Optional[str] = Form(None),
                      completed: bool = Form(False), tag: Optional[TodoStatus] = Form(None),
                      database: Session = Depends(get_db)):
    """Update todo"""
    todo = database.query(TodoItem).get(todo_id)
    if not todo:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Todo not found")
    todo.title = title
    todo.details = details
    todo.completed = completed
    todo.tag = tag
    database.commit()
    return RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@app.post("/update/{todo_id}")
async def todo_update(request: Request, todo_id: int, completed: bool = Form(False),
                      database: Session = Depends(get_db)):
    """Update todo status"""
    todo = database.query(TodoItem).get(todo_id)
    if not todo:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Todo not found")
    todo.completed = completed
    database.commit()
    return RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@app.get("/delete/{todo_id}")
async def todo_delete(request: Request, todo_id: int, database: Session = Depends(get_db)):
    """Delete todo"""
    try:
        todo = database.query(TodoItem).get(todo_id)
        if not todo:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Todo not found")
        database.delete(todo)
        database.commit()
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(e))

    return RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


if __name__ == '__main__':
    uvicorn.run(app, host="localhost", port=80, log_level="info")

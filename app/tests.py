# import unittest
#
# from fastapi.testclient import TestClient
# import requests
#
# from main import app
#
#
# class TestTodoApp(unittest.TestCase):
#     def setUp(self):
#         self.client = TestClient(app)
#
#     def test_home(self):
#         response = self.client.get("/")
#         self.assertEqual(response.status_code, 200)  # Add more assertions to validate the response content
#
#     def test_todo_add(self):
#         todo_data = {"title": "Test Todo", "details": "This is a test todo", "tag": "STUDY"}
#         response = self.client.post("/add", data=todo_data)
#         self.assertEqual(response.status_code, 303)  # Add more assertions to validate the response content
#
#     def test_todo_edit(self):
#         todo_id = 1  # Replace with an existing todo ID
#         response = self.client.get(f"/edit/{todo_id}")
#         self.assertEqual(response.status_code, 200)  # Add more assertions to validate the response content
#
#     def test_todo_update(self):
#         todo_id = 1  # Replace with an existing todo ID
#         todo_data = {"title": "Updated Todo", "details": "This is an updated todo", "completed": True,
#                      "tag": "PERSONAL"}
#         response = self.client.post(f"/edit/{todo_id}", data=todo_data)
#         self.assertEqual(response.status_code, 303)  # Add more assertions to validate the response content
#
#     def test_todo_update_status(self):
#         todo_id = 1  # Replace with an existing todo ID
#         todo_data = {"completed": True}
#         response = self.client.post(f"/update/{todo_id}", data=todo_data)
#         self.assertEqual(response.status_code, 303)  # Add more assertions to validate the response content
#
#     def test_todo_delete(self):
#         todo_id = 1  # Replace with an existing todo ID
#         response = self.client.get(f"/delete/{todo_id}")
#         self.assertEqual(response.status_code, 303)  # Add more assertions to validate the response content
#
#
# if __name__ == "__main__":
#     unittest.main()

```Dockerfile
# Задаем базовый образ (можешь использовать образ Python с предустановленным fastapi)
FROM python:3.11

# Устанавливаем зависимости из requirements.txt
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Задаем рабочую директорию
WORKDIR /app

# Копируем исходный код в контейнер
COPY . .

# Запускаем контейнер в режиме разработчика
CMD ["uvicorn", "main:app", "--reload"]

# Устанавливаем переменную окружения для режима пользователя
ENV ENV=USER

# Команда для запуска контейнера в режиме пользователя
CMD ["uvicorn", "main:app"]
```

После создания Dockerfile выполните команду docker build -t my-app . для создания образа.
Затем вы можете запустить контейнер, указав режим работы с помощью переменных окружения.
Например, для запуска контейнера в режиме разработчика:

   ```bash
   docker run -e ENV=DEVELOPER my-app
   ```

Для запуска контейнера в режиме пользователя:

   ```bash
   docker run -e ENV=USER my-app
   ```
import random

from main import TodoItem, SESSIONLOCAL


def generate_random_todos():
    titles = ["Buy groceries", "Finish homework", "Call mom", "Go for a run", "Read a book", "Clean the house",
              "Write a blog post", "Attend a meeting", "Fix the bug", "Plan a trip", "Learn a new skill",
              "Watch a movie", "Organize files", "Cook dinner", "Start a new project", "Exercise", "Visit a friend",
              "Take a nap", "Listen to music", "Write code"]

    session = SESSIONLOCAL()
    for _ in range(20):
        title = random.choice(titles)
        todo = TodoItem(title=title, details="", completed=False, tag="STUDY")
        session.add(todo)
    session.commit()


if __name__ == '__main__':
    generate_random_todos()
